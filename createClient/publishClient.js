const SNSClient = require("aws-sdk/clients/sns");
const sns = new SNSClient({ region: "us-east-1" });

const publishClient = async (event, topicArn) => {
  let params = {
    Message: JSON.stringify(event),
    TopicArn: topicArn,
  };
  await sns.publish(params).promise();
};

module.exports = publishClient;
