const DYNAMODB = require("aws-sdk/clients/dynamodb");
const dynamodb = new DYNAMODB({ region: "us-east-1" });

const eventToParams = (event) => {
  if (typeof event !== "object") throw Error("Event is not an object");
  let keys = Object.keys(event);
  let dataType = {
    string: "S",
    number: "N",
  };
  return keys.reduce((acum, key) => {
    let valueType = typeof event[key];
    let value = {};
    value[dataType[valueType]] = event[key];
    acum[key] = value;
    return { ...acum };
  }, {});
};

const saveEvent = async (event, tableName) => {
  const params = {
    Item: eventToParams(event),
    TableName: tableName,
  };
  await dynamodb.putItem(params).promise();
};

module.exports = saveEvent;
