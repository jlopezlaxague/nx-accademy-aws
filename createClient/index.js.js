const saveEvent = require("./saveClient");
const publishClient = require("./publishClient");
const dynamoTableName = "JLopezLaxague-clients";
const snsTopicArn =
  "arn:aws:sns:us-east-1:450865910417:JLopezLaxague-createClient";

exports.handler = async (event) => {
  try {
    await saveEvent(event, dynamoTableName);
    await publishClient(event, snsTopicArn);
    const response = {
      statusCode: 200,
      body: JSON.stringify("Client saved and published!"),
    };
    return response;
  } catch (e) {
    const response = {
      statusCode: 500,
      body: `Error in client creation ${e}`,
    };
    return response;
  }
};
