const DYNAMODB = require("aws-sdk/clients/dynamodb");
const dynamodb = new DYNAMODB({ region: "us-east-1" });
const createCard = require("./createCard");

const updateClient = (event, tableName) => {
  let payload = JSON.parse(JSON.parse(event?.Records[0]?.body)?.Message);
  let params = {
    ExpressionAttributeNames: {
      "#C": "creditCard",
    },
    ExpressionAttributeValues: {
      ":c": {
        M: createCard(payload),
      },
    },
    Key: {
      dni: {
        S: payload?.dni,
      },
    },
    ReturnValues: "ALL_NEW",
    TableName: tableName,
    UpdateExpression: "SET #C = :c",
  };
  return dynamodb.updateItem(params).promise();
};

module.exports = updateClient;
