const getAge = (date) => {
  let [day, month, year] = date.split("/");
  let currentDate = new Date();
  let birthDate = new Date([month, day, year].join("/"));
  return (currentDate - birthDate) / 31557600000;
};

const generateRandom = (digits) => {
  let count = 1;
  let resp = [];
  while (count <= digits) {
    resp.push(Math.floor(Math.random() * (9 + 1)));
    count++;
  }
  return resp.join("");
};

const getExpDate = () => {
  let currentDate = new Date();
  let resp = [
    currentDate.getDate(),
    currentDate.getMonth() + 1,
    currentDate.getFullYear() + 5,
  ].join("/");
  return resp;
};

const createCard = (payload) => {
  try {
    console.log("payload", payload);
    let birthDate = payload?.birthDate;
    let cardNumber = generateRandom(16);
    let cvv = generateRandom(3);
    let expDate = getExpDate();
    let age = getAge(birthDate);
    let type = age < 45 ? "Classic" : "Gold";
    let resp = {
      type: { S: type },
      number: { S: cardNumber },
      expDate: { S: expDate },
      cvv: { S: cvv },
    };
    return resp;
  } catch (e) {
    throw Error(`Error in card creation ${e}`);
  }
};

module.exports = createCard;
