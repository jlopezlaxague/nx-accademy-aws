const updateClient = require("./updateClient");
const dynamoTableName = "JLopezLaxague-clients";

exports.handler = async (event) => {
  try {
    await updateClient(event, dynamoTableName);
    const response = {
      statusCode: 200,
      body: JSON.stringify("Client updated"),
    };
    return response;
  } catch (e) {
    const response = {
      statusCode: 500,
      body: `Error in gift creation ${e}`,
    };
    return response;
  }
};
