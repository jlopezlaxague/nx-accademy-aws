const dictGift = {
  1: "remera",
  2: "remera",
  3: "remera",
  4: "buzo",
  5: "buzo",
  6: "buzo",
  7: "sweater",
  8: "sweater",
  9: "sweater",
  10: "camisa",
  11: "camisa",
  12: "camisa",
};

const getMonth = (date) => {
  let [day, month, year] = date.split("/");
  let birthDate = new Date([month, day, year].join("/"));
  return birthDate.getMonth() + 1;
};

const createGift = (payload) => {
  try {
    let birthDate = payload?.birthDate;
    let resp = dictGift[getMonth(birthDate)];
    console.log("Gift==>", resp);
    return resp;
  } catch (e) {
    throw Error(`Error in card creation ${e}`);
  }
};

module.exports = createGift;
