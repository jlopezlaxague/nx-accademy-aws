const DYNAMODB = require("aws-sdk/clients/dynamodb");
const dynamodb = new DYNAMODB({ region: "us-east-1" });
const createGift = require("./createGift");

const updateClient = (event, tableName) => {
  let payload = JSON.parse(JSON.parse(event?.Records[0]?.body)?.Message);
  let params = {
    ExpressionAttributeNames: {
      "#C": "gift",
    },
    ExpressionAttributeValues: {
      ":c": {
        S: createGift(payload),
      },
    },
    Key: {
      dni: {
        S: payload?.dni,
      },
    },
    ReturnValues: "ALL_NEW",
    TableName: tableName,
    UpdateExpression: "SET #C = :c",
  };
  return dynamodb.updateItem(params).promise();
};

module.exports = updateClient;
